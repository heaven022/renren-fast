package io.csl.modules.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_promotion_type")
public class PromotionType {
    @Id
    @Column(name = "pro_id")
    private Long proId;

    /**
     * 优惠金额
     */
    private BigDecimal amount;

    /**
     * 优惠券类型：1 优惠金额 2 全免 3 折扣
     */
    private String type;

    /**
     * 有效期：天
     */
    private Integer exipredate;

    /**
     * 状态
     */
    private String status;

    /**
     * 参数
     */
    private String field1;

    /**
     * 参数
     */
    private String field2;

    /**
     * 参数
     */
    private String field3;

    /**
     * 参数
     */
    private String field4;

    /**
     * 参数
     */
    private String field5;

    /**
     * 类型
     */
    @Column(name = "car_type")
    private String carType;

    /**
     * 开始时间
     */
    @Column(name = "start_date")
    private Date startDate;

    /**
     * 结束时间
     */
    @Column(name = "end_date")
    private Date endDate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return pro_id
     */
    public Long getProId() {
        return proId;
    }

    /**
     * @param proId
     */
    public void setProId(Long proId) {
        this.proId = proId;
    }

    /**
     * 获取优惠金额
     *
     * @return amount - 优惠金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 设置优惠金额
     *
     * @param amount 优惠金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取优惠券类型：1 优惠金额 2 全免 3 折扣
     *
     * @return type - 优惠券类型：1 优惠金额 2 全免 3 折扣
     */
    public String getType() {
        return type;
    }

    /**
     * 设置优惠券类型：1 优惠金额 2 全免 3 折扣
     *
     * @param type 优惠券类型：1 优惠金额 2 全免 3 折扣
     */
    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    /**
     * 获取有效期：天
     *
     * @return exipredate - 有效期：天
     */
    public Integer getExipredate() {
        return exipredate;
    }

    /**
     * 设置有效期：天
     *
     * @param exipredate 有效期：天
     */
    public void setExipredate(Integer exipredate) {
        this.exipredate = exipredate;
    }

    /**
     * 获取状态
     *
     * @return status - 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取参数
     *
     * @return field1 - 参数
     */
    public String getField1() {
        return field1;
    }

    /**
     * 设置参数
     *
     * @param field1 参数
     */
    public void setField1(String field1) {
        this.field1 = field1 == null ? null : field1.trim();
    }

    /**
     * 获取参数
     *
     * @return field2 - 参数
     */
    public String getField2() {
        return field2;
    }

    /**
     * 设置参数
     *
     * @param field2 参数
     */
    public void setField2(String field2) {
        this.field2 = field2 == null ? null : field2.trim();
    }

    /**
     * 获取参数
     *
     * @return field3 - 参数
     */
    public String getField3() {
        return field3;
    }

    /**
     * 设置参数
     *
     * @param field3 参数
     */
    public void setField3(String field3) {
        this.field3 = field3 == null ? null : field3.trim();
    }

    /**
     * 获取参数
     *
     * @return field4 - 参数
     */
    public String getField4() {
        return field4;
    }

    /**
     * 设置参数
     *
     * @param field4 参数
     */
    public void setField4(String field4) {
        this.field4 = field4 == null ? null : field4.trim();
    }

    /**
     * 获取参数
     *
     * @return field5 - 参数
     */
    public String getField5() {
        return field5;
    }

    /**
     * 设置参数
     *
     * @param field5 参数
     */
    public void setField5(String field5) {
        this.field5 = field5 == null ? null : field5.trim();
    }

    /**
     * 获取类型
     *
     * @return car_type - 类型
     */
    public String getCarType() {
        return carType;
    }

    /**
     * 设置类型
     *
     * @param carType 类型
     */
    public void setCarType(String carType) {
        this.carType = carType == null ? null : carType.trim();
    }

    /**
     * 获取开始时间
     *
     * @return start_date - 开始时间
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * 设置开始时间
     *
     * @param startDate 开始时间
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 获取结束时间
     *
     * @return end_date - 结束时间
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * 设置结束时间
     *
     * @param endDate 结束时间
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}