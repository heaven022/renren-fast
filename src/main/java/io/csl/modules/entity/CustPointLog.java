package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_customer_point_log")
public class CustPointLog {
    @Id
    @Column(name = "point_id")
    private Long pointId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 来源：0 订单 1 登录 2活动 3充值
     */
    private String source;

    /**
     * 来源标识编号
     */
    @Column(name = "refer_number")
    private String referNumber;

    /**
     * 变更积分数
     */
    @Column(name = "change_point")
    private Integer changePoint;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * @return point_id
     */
    public Long getPointId() {
        return pointId;
    }

    /**
     * @param pointId
     */
    public void setPointId(Long pointId) {
        this.pointId = pointId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取来源：0 订单 1 登录 2活动 3充值
     *
     * @return source - 来源：0 订单 1 登录 2活动 3充值
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置来源：0 订单 1 登录 2活动 3充值
     *
     * @param source 来源：0 订单 1 登录 2活动 3充值
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 获取来源标识编号
     *
     * @return refer_number - 来源标识编号
     */
    public String getReferNumber() {
        return referNumber;
    }

    /**
     * 设置来源标识编号
     *
     * @param referNumber 来源标识编号
     */
    public void setReferNumber(String referNumber) {
        this.referNumber = referNumber == null ? null : referNumber.trim();
    }

    /**
     * 获取变更积分数
     *
     * @return change_point - 变更积分数
     */
    public Integer getChangePoint() {
        return changePoint;
    }

    /**
     * 设置变更积分数
     *
     * @param changePoint 变更积分数
     */
    public void setChangePoint(Integer changePoint) {
        this.changePoint = changePoint;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }
}