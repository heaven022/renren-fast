package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_customer_promotion_rel")
public class CustPromotionRelation {
    @Id
    private Long rid;

    /**
     * 优惠券类型id
     */
    private Long pid;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 第三方标识
     */
    @Column(name = "third_part_id")
    private String thirdPartId;

    /**
     * 优惠券码：唯一
     */
    @Column(name = "promotion_code")
    private String promotionCode;

    /**
     * 状态
     */
    private String status;

    /**
     * 关联订单号
     */
    @Column(name = "reference_sn")
    private String referenceSn;

    /**
     * 备注
     */
    private String remark;

    /**
     * 使用时间
     */
    @Column(name = "used_date")
    private Date usedDate;

    /**
     * 过期时间
     */
    @Column(name = "expire_date")
    private Date expireDate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return rid
     */
    public Long getRid() {
        return rid;
    }

    /**
     * @param rid
     */
    public void setRid(Long rid) {
        this.rid = rid;
    }

    /**
     * 获取优惠券类型id
     *
     * @return pid - 优惠券类型id
     */
    public Long getPid() {
        return pid;
    }

    /**
     * 设置优惠券类型id
     *
     * @param pid 优惠券类型id
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取第三方标识
     *
     * @return third_part_id - 第三方标识
     */
    public String getThirdPartId() {
        return thirdPartId;
    }

    /**
     * 设置第三方标识
     *
     * @param thirdPartId 第三方标识
     */
    public void setThirdPartId(String thirdPartId) {
        this.thirdPartId = thirdPartId == null ? null : thirdPartId.trim();
    }

    /**
     * 获取优惠券码：唯一
     *
     * @return promotion_code - 优惠券码：唯一
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     * 设置优惠券码：唯一
     *
     * @param promotionCode 优惠券码：唯一
     */
    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode == null ? null : promotionCode.trim();
    }

    /**
     * 获取状态
     *
     * @return status - 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取关联订单号
     *
     * @return reference_sn - 关联订单号
     */
    public String getReferenceSn() {
        return referenceSn;
    }

    /**
     * 设置关联订单号
     *
     * @param referenceSn 关联订单号
     */
    public void setReferenceSn(String referenceSn) {
        this.referenceSn = referenceSn == null ? null : referenceSn.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取使用时间
     *
     * @return used_date - 使用时间
     */
    public Date getUsedDate() {
        return usedDate;
    }

    /**
     * 设置使用时间
     *
     * @param usedDate 使用时间
     */
    public void setUsedDate(Date usedDate) {
        this.usedDate = usedDate;
    }

    /**
     * 获取过期时间
     *
     * @return expire_date - 过期时间
     */
    public Date getExpireDate() {
        return expireDate;
    }

    /**
     * 设置过期时间
     *
     * @param expireDate 过期时间
     */
    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}