package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_device")
public class Device {
    @Id
    @Column(name = "dev_id")
    private Long devId;

    /**
     * 硬件类型
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 状态
     */
    @Column(name = "device_status")
    private String deviceStatus;

    /**
     * 所属门店
     */
    @Column(name = "org_id")
    private Long orgId;

    /**
     * 设备参数
     */
    private String field1;

    /**
     * 设备参数
     */
    private String field2;

    /**
     * 设备参数
     */
    private String field3;

    /**
     * 设备参数
     */
    private String field4;

    /**
     * 设备参数
     */
    private String field5;

    /**
     * 监控URL
     */
    @Column(name = "URL")
    private String url;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return dev_id
     */
    public Long getDevId() {
        return devId;
    }

    /**
     * @param devId
     */
    public void setDevId(Long devId) {
        this.devId = devId;
    }

    /**
     * 获取硬件类型
     *
     * @return device_type - 硬件类型
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * 设置硬件类型
     *
     * @param deviceType 硬件类型
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType == null ? null : deviceType.trim();
    }

    /**
     * 获取状态
     *
     * @return device_status - 状态
     */
    public String getDeviceStatus() {
        return deviceStatus;
    }

    /**
     * 设置状态
     *
     * @param deviceStatus 状态
     */
    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus == null ? null : deviceStatus.trim();
    }

    /**
     * 获取所属门店
     *
     * @return org_id - 所属门店
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 设置所属门店
     *
     * @param orgId 所属门店
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取设备参数
     *
     * @return field1 - 设备参数
     */
    public String getField1() {
        return field1;
    }

    /**
     * 设置设备参数
     *
     * @param field1 设备参数
     */
    public void setField1(String field1) {
        this.field1 = field1 == null ? null : field1.trim();
    }

    /**
     * 获取设备参数
     *
     * @return field2 - 设备参数
     */
    public String getField2() {
        return field2;
    }

    /**
     * 设置设备参数
     *
     * @param field2 设备参数
     */
    public void setField2(String field2) {
        this.field2 = field2 == null ? null : field2.trim();
    }

    /**
     * 获取设备参数
     *
     * @return field3 - 设备参数
     */
    public String getField3() {
        return field3;
    }

    /**
     * 设置设备参数
     *
     * @param field3 设备参数
     */
    public void setField3(String field3) {
        this.field3 = field3 == null ? null : field3.trim();
    }

    /**
     * 获取设备参数
     *
     * @return field4 - 设备参数
     */
    public String getField4() {
        return field4;
    }

    /**
     * 设置设备参数
     *
     * @param field4 设备参数
     */
    public void setField4(String field4) {
        this.field4 = field4 == null ? null : field4.trim();
    }

    /**
     * 获取设备参数
     *
     * @return field5 - 设备参数
     */
    public String getField5() {
        return field5;
    }

    /**
     * 设置设备参数
     *
     * @param field5 设备参数
     */
    public void setField5(String field5) {
        this.field5 = field5 == null ? null : field5.trim();
    }

    /**
     * 获取监控URL
     *
     * @return URL - 监控URL
     */
    public String getUrl() {
        return url;
    }

    /**
     * 设置监控URL
     *
     * @param url 监控URL
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}