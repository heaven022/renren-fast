package io.csl.modules.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_order")
public class Order {
    @Id
    private Long oid;

    /**
     * 交易商铺
     */
    @Column(name = "store_id")
    private Long storeId;

    /**
     * 交易设备
     */
    @Column(name = "device_id")
    private String deviceId;

    /**
     * 优惠券的主键
     */
    @Column(name = "pro_r_id")
    private Long proRId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 第三方标识
     */
    @Column(name = "third_part_id")
    private String thirdPartId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 内部交易id
     */
    @Column(name = "txn_id")
    private String txnId;

    /**
     * 交易方式类型
     */
    @Column(name = "payment_method")
    private String paymentMethod;

    /**
     * 订单类型
     */
    @Column(name = "order_type")
    private String orderType;

    /**
     * 交易描述
     */
    @Column(name = "txn_desp")
    private String txnDesp;

    /**
     * 订单总金额
     */
    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 使用余额结算金额
     */
    private BigDecimal integral;

    /**
     * 使用优惠券结算金额
     */
    @Column(name = "promotion_amount")
    private BigDecimal promotionAmount;

    /**
     * 第三方支付需要结算金额
     */
    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    /**
     * 是否开过发票
     */
    private String invoice;

    /**
     * 优惠券码：唯一
     */
    @Column(name = "promotion_code")
    private String promotionCode;

    /**
     * 状态
     */
    private String status;

    /**
     * 第三方关联订单号
     */
    @Column(name = "reference_sn")
    private String referenceSn;

    /**
     * 错误代码
     */
    @Column(name = "error_code")
    private String errorCode;

    /**
     * 错误描述
     */
    @Column(name = "error_desc")
    private String errorDesc;

    /**
     * 参数
     */
    private String field1;

    /**
     * 参数
     */
    private String field2;

    /**
     * 参数
     */
    private String field3;

    /**
     * 参数
     */
    private String field4;

    /**
     * 参数
     */
    private String field5;

    /**
     * 对账结算时间
     */
    @Column(name = "closed_date")
    private Date closedDate;

    /**
     * 交易完成时间
     */
    @Column(name = "txn_date")
    private Date txnDate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return oid
     */
    public Long getOid() {
        return oid;
    }

    /**
     * @param oid
     */
    public void setOid(Long oid) {
        this.oid = oid;
    }

    /**
     * 获取交易商铺
     *
     * @return store_id - 交易商铺
     */
    public Long getStoreId() {
        return storeId;
    }

    /**
     * 设置交易商铺
     *
     * @param storeId 交易商铺
     */
    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    /**
     * 获取交易设备
     *
     * @return device_id - 交易设备
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 设置交易设备
     *
     * @param deviceId 交易设备
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId == null ? null : deviceId.trim();
    }

    /**
     * 获取优惠券的主键
     *
     * @return pro_r_id - 优惠券的主键
     */
    public Long getProRId() {
        return proRId;
    }

    /**
     * 设置优惠券的主键
     *
     * @param proRId 优惠券的主键
     */
    public void setProRId(Long proRId) {
        this.proRId = proRId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取第三方标识
     *
     * @return third_part_id - 第三方标识
     */
    public String getThirdPartId() {
        return thirdPartId;
    }

    /**
     * 设置第三方标识
     *
     * @param thirdPartId 第三方标识
     */
    public void setThirdPartId(String thirdPartId) {
        this.thirdPartId = thirdPartId == null ? null : thirdPartId.trim();
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    /**
     * 获取内部交易id
     *
     * @return txn_id - 内部交易id
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * 设置内部交易id
     *
     * @param txnId 内部交易id
     */
    public void setTxnId(String txnId) {
        this.txnId = txnId == null ? null : txnId.trim();
    }

    /**
     * 获取交易方式类型
     *
     * @return payment_method - 交易方式类型
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * 设置交易方式类型
     *
     * @param paymentMethod 交易方式类型
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod == null ? null : paymentMethod.trim();
    }

    /**
     * 获取订单类型
     *
     * @return order_type - 订单类型
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * 设置订单类型
     *
     * @param orderType 订单类型
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType == null ? null : orderType.trim();
    }

    /**
     * 获取交易描述
     *
     * @return txn_desp - 交易描述
     */
    public String getTxnDesp() {
        return txnDesp;
    }

    /**
     * 设置交易描述
     *
     * @param txnDesp 交易描述
     */
    public void setTxnDesp(String txnDesp) {
        this.txnDesp = txnDesp == null ? null : txnDesp.trim();
    }

    /**
     * 获取订单总金额
     *
     * @return order_amount - 订单总金额
     */
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    /**
     * 设置订单总金额
     *
     * @param orderAmount 订单总金额
     */
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * 获取使用余额结算金额
     *
     * @return integral - 使用余额结算金额
     */
    public BigDecimal getIntegral() {
        return integral;
    }

    /**
     * 设置使用余额结算金额
     *
     * @param integral 使用余额结算金额
     */
    public void setIntegral(BigDecimal integral) {
        this.integral = integral;
    }

    /**
     * 获取使用优惠券结算金额
     *
     * @return promotion_amount - 使用优惠券结算金额
     */
    public BigDecimal getPromotionAmount() {
        return promotionAmount;
    }

    /**
     * 设置使用优惠券结算金额
     *
     * @param promotionAmount 使用优惠券结算金额
     */
    public void setPromotionAmount(BigDecimal promotionAmount) {
        this.promotionAmount = promotionAmount;
    }

    /**
     * 获取第三方支付需要结算金额
     *
     * @return payment_amount - 第三方支付需要结算金额
     */
    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    /**
     * 设置第三方支付需要结算金额
     *
     * @param paymentAmount 第三方支付需要结算金额
     */
    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    /**
     * 获取是否开过发票
     *
     * @return invoice - 是否开过发票
     */
    public String getInvoice() {
        return invoice;
    }

    /**
     * 设置是否开过发票
     *
     * @param invoice 是否开过发票
     */
    public void setInvoice(String invoice) {
        this.invoice = invoice == null ? null : invoice.trim();
    }

    /**
     * 获取优惠券码：唯一
     *
     * @return promotion_code - 优惠券码：唯一
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     * 设置优惠券码：唯一
     *
     * @param promotionCode 优惠券码：唯一
     */
    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode == null ? null : promotionCode.trim();
    }

    /**
     * 获取状态
     *
     * @return status - 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * 获取第三方关联订单号
     *
     * @return reference_sn - 第三方关联订单号
     */
    public String getReferenceSn() {
        return referenceSn;
    }

    /**
     * 设置第三方关联订单号
     *
     * @param referenceSn 第三方关联订单号
     */
    public void setReferenceSn(String referenceSn) {
        this.referenceSn = referenceSn == null ? null : referenceSn.trim();
    }

    /**
     * 获取错误代码
     *
     * @return error_code - 错误代码
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * 设置错误代码
     *
     * @param errorCode 错误代码
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode == null ? null : errorCode.trim();
    }

    /**
     * 获取错误描述
     *
     * @return error_desc - 错误描述
     */
    public String getErrorDesc() {
        return errorDesc;
    }

    /**
     * 设置错误描述
     *
     * @param errorDesc 错误描述
     */
    public void setErrorDesc(String errorDesc) {
        this.errorDesc = errorDesc == null ? null : errorDesc.trim();
    }

    /**
     * 获取参数
     *
     * @return field1 - 参数
     */
    public String getField1() {
        return field1;
    }

    /**
     * 设置参数
     *
     * @param field1 参数
     */
    public void setField1(String field1) {
        this.field1 = field1 == null ? null : field1.trim();
    }

    /**
     * 获取参数
     *
     * @return field2 - 参数
     */
    public String getField2() {
        return field2;
    }

    /**
     * 设置参数
     *
     * @param field2 参数
     */
    public void setField2(String field2) {
        this.field2 = field2 == null ? null : field2.trim();
    }

    /**
     * 获取参数
     *
     * @return field3 - 参数
     */
    public String getField3() {
        return field3;
    }

    /**
     * 设置参数
     *
     * @param field3 参数
     */
    public void setField3(String field3) {
        this.field3 = field3 == null ? null : field3.trim();
    }

    /**
     * 获取参数
     *
     * @return field4 - 参数
     */
    public String getField4() {
        return field4;
    }

    /**
     * 设置参数
     *
     * @param field4 参数
     */
    public void setField4(String field4) {
        this.field4 = field4 == null ? null : field4.trim();
    }

    /**
     * 获取参数
     *
     * @return field5 - 参数
     */
    public String getField5() {
        return field5;
    }

    /**
     * 设置参数
     *
     * @param field5 参数
     */
    public void setField5(String field5) {
        this.field5 = field5 == null ? null : field5.trim();
    }

    /**
     * 获取对账结算时间
     *
     * @return closed_date - 对账结算时间
     */
    public Date getClosedDate() {
        return closedDate;
    }

    /**
     * 设置对账结算时间
     *
     * @param closedDate 对账结算时间
     */
    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    /**
     * 获取交易完成时间
     *
     * @return txn_date - 交易完成时间
     */
    public Date getTxnDate() {
        return txnDate;
    }

    /**
     * 设置交易完成时间
     *
     * @param txnDate 交易完成时间
     */
    public void setTxnDate(Date txnDate) {
        this.txnDate = txnDate;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}