package io.csl.modules.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tb_car")
public class Car {
	@Id
	@Column(name = "car_id")
	private Long carId;

	/**
	 * 用户id
	 */
	@Column(name = "user_id")
	private Long userId;

	/**
	 * 车牌号
	 */
	@Column(name = "car_license_num")
	private String carLicenseNum;

	/**
	 * 子品牌
	 */
	@Column(name = "car_brand")
	private String carBrand;

	/**
	 * 父品牌
	 */
	@Column(name = "car_company")
	private String carCompany;

	/**
	 * 类型
	 */
	@Column(name = "car_type")
	private String carType;

	/**
	 * 是否为主汽车
	 */
	@Column(name = "major_car")
	private String majorCar;

	/**
	 * 状态
	 */
	private String status;

	/**
	 * 第三方标识
	 */
	@Column(name = "third_part_id")
	private String thirdPartId;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 创建人
	 */
	@Column(name = "create_by")
	private String createBy;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 更新人
	 */
	@Column(name = "update_by")
	private String updateBy;

	/**
	 * @return car_id
	 */
	public Long getCarId() {
		return carId;
	}

	/**
	 * @param carId
	 */
	public void setCarId(Long carId) {
		this.carId = carId;
	}

	/**
	 * 获取用户id
	 *
	 * @return user_id - 用户id
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * 设置用户id
	 *
	 * @param userId
	 *            用户id
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * 获取车牌号
	 *
	 * @return car_license_num - 车牌号
	 */
	public String getCarLicenseNum() {
		return carLicenseNum;
	}

	/**
	 * 设置车牌号
	 *
	 * @param carLicenseNum
	 *            车牌号
	 */
	public void setCarLicenseNum(String carLicenseNum) {
		this.carLicenseNum = carLicenseNum == null ? null : carLicenseNum.trim();
	}

	/**
	 * 获取子品牌
	 *
	 * @return car_brand - 子品牌
	 */
	public String getCarBrand() {
		return carBrand;
	}

	/**
	 * 设置子品牌
	 *
	 * @param carBrand
	 *            子品牌
	 */
	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand == null ? null : carBrand.trim();
	}

	/**
	 * 获取父品牌
	 *
	 * @return car_company - 父品牌
	 */
	public String getCarCompany() {
		return carCompany;
	}

	/**
	 * 设置父品牌
	 *
	 * @param carCompany
	 *            父品牌
	 */
	public void setCarCompany(String carCompany) {
		this.carCompany = carCompany == null ? null : carCompany.trim();
	}

	/**
	 * 获取类型
	 *
	 * @return car_type - 类型
	 */
	public String getCarType() {
		return carType;
	}

	/**
	 * 设置类型
	 *
	 * @param carType
	 *            类型
	 */
	public void setCarType(String carType) {
		this.carType = carType == null ? null : carType.trim();
	}

	/**
	 * 获取是否为主汽车
	 *
	 * @return major_car - 是否为主汽车
	 */
	public String getMajorCar() {
		return majorCar;
	}

	/**
	 * 设置是否为主汽车
	 *
	 * @param majorCar
	 *            是否为主汽车
	 */
	public void setMajorCar(String majorCar) {
		this.majorCar = majorCar == null ? null : majorCar.trim();
	}

	/**
	 * 获取状态
	 *
	 * @return status - 状态
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 设置状态
	 *
	 * @param status
	 *            状态
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	/**
	 * 获取第三方标识
	 *
	 * @return third_part_id - 第三方标识
	 */
	public String getThirdPartId() {
		return thirdPartId;
	}

	/**
	 * 设置第三方标识
	 *
	 * @param thirdPartId
	 *            第三方标识
	 */
	public void setThirdPartId(String thirdPartId) {
		this.thirdPartId = thirdPartId == null ? null : thirdPartId.trim();
	}

	/**
	 * 获取创建时间
	 *
	 * @return create_time - 创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createTime
	 *            创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取创建人
	 *
	 * @return create_by - 创建人
	 */
	public String getCreateBy() {
		return createBy;
	}

	/**
	 * 设置创建人
	 *
	 * @param createBy
	 *            创建人
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy == null ? null : createBy.trim();
	}

	/**
	 * 获取更新时间
	 *
	 * @return update_time - 更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * 设置更新时间
	 *
	 * @param updateTime
	 *            更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * 获取更新人
	 *
	 * @return update_by - 更新人
	 */
	public String getUpdateBy() {
		return updateBy;
	}

	/**
	 * 设置更新人
	 *
	 * @param updateBy
	 *            更新人
	 */
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy == null ? null : updateBy.trim();
	}
}