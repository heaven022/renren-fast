package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_customer_level_inf")
public class CustLevelInfo {
    @Id
    @Column(name = "lvl_id")
    private Long lvlId;

    /**
     * 级别名称
     */
    @Column(name = "level_name")
    private String levelName;

    /**
     * 该级别最低积分
     */
    @Column(name = "min_point")
    private Integer minPoint;

    /**
     * 该级别最高积分
     */
    @Column(name = "max_point")
    private Integer maxPoint;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return lvl_id
     */
    public Long getLvlId() {
        return lvlId;
    }

    /**
     * @param lvlId
     */
    public void setLvlId(Long lvlId) {
        this.lvlId = lvlId;
    }

    /**
     * 获取级别名称
     *
     * @return level_name - 级别名称
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * 设置级别名称
     *
     * @param levelName 级别名称
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName == null ? null : levelName.trim();
    }

    /**
     * 获取该级别最低积分
     *
     * @return min_point - 该级别最低积分
     */
    public Integer getMinPoint() {
        return minPoint;
    }

    /**
     * 设置该级别最低积分
     *
     * @param minPoint 该级别最低积分
     */
    public void setMinPoint(Integer minPoint) {
        this.minPoint = minPoint;
    }

    /**
     * 获取该级别最高积分
     *
     * @return max_point - 该级别最高积分
     */
    public Integer getMaxPoint() {
        return maxPoint;
    }

    /**
     * 设置该级别最高积分
     *
     * @param maxPoint 该级别最高积分
     */
    public void setMaxPoint(Integer maxPoint) {
        this.maxPoint = maxPoint;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}