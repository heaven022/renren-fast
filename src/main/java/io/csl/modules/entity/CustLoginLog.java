package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_customer_login_log")
public class CustLoginLog {
    @Id
    @Column(name = "login_id")
    private Long loginId;

    /**
     * 用户id
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 登录时间
     */
    @Column(name = "login_time")
    private Date loginTime;

    /**
     * IP
     */
    @Column(name = "login_ip")
    private String loginIp;

    /**
     * 登录类型
     */
    @Column(name = "login_type")
    private String loginType;

    /**
     * 登出
     */
    @Column(name = "logout_time")
    private Date logoutTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * @return login_id
     */
    public Long getLoginId() {
        return loginId;
    }

    /**
     * @param loginId
     */
    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    /**
     * 获取用户id
     *
     * @return user_id - 用户id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 设置用户id
     *
     * @param userId 用户id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取登录时间
     *
     * @return login_time - 登录时间
     */
    public Date getLoginTime() {
        return loginTime;
    }

    /**
     * 设置登录时间
     *
     * @param loginTime 登录时间
     */
    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * 获取IP
     *
     * @return login_ip - IP
     */
    public String getLoginIp() {
        return loginIp;
    }

    /**
     * 设置IP
     *
     * @param loginIp IP
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    /**
     * 获取登录类型
     *
     * @return login_type - 登录类型
     */
    public String getLoginType() {
        return loginType;
    }

    /**
     * 设置登录类型
     *
     * @param loginType 登录类型
     */
    public void setLoginType(String loginType) {
        this.loginType = loginType == null ? null : loginType.trim();
    }

    /**
     * 获取登出
     *
     * @return logout_time - 登出
     */
    public Date getLogoutTime() {
        return logoutTime;
    }

    /**
     * 设置登出
     *
     * @param logoutTime 登出
     */
    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }
}