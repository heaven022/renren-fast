package io.csl.modules.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_user")
public class User {
    @Id
    @Column(name = "user_id")
    private Long userId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码
     */
    private String password;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 第三方id
     */
    @Column(name = "third_part_id")
    private String thirdPartId;

    /**
     * 用户来源
     */
    @Column(name = "source_channel")
    private String sourceChannel;

    /**
     * 用户姓名
     */
    private String customername;

    /**
     * 用户姓别
     */
    private String gender;

    /**
     * 婚姻状态
     */
    private String marital;

    /**
     * 生日
     */
    private Date birth;

    /**
     * 所在省
     */
    private String province;

    /**
     * 所在城市
     */
    private String city;

    /**
     * 所在地区
     */
    private String district;

    /**
     * 具体地址
     */
    private String address;

    /**
     * 邮编
     */
    private String zip;

    /**
     * 注册时间
     */
    @Column(name = "register_time")
    private Date registerTime;

    /**
     * 上次登录时间
     */
    @Column(name = "last_login_time")
    private Date lastLoginTime;

    /**
     * 积分
     */
    @Column(name = "user_point")
    private Integer userPoint;

    /**
     * 用户余额
     */
    @Column(name = "user_money")
    private BigDecimal userMoney;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * @return user_id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 获取用户名
     *
     * @return username - 用户名
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置用户名
     *
     * @param username 用户名
     */
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    /**
     * 获取手机号
     *
     * @return mobile - 手机号
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设置手机号
     *
     * @param mobile 手机号
     */
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取第三方id
     *
     * @return third_part_id - 第三方id
     */
    public String getThirdPartId() {
        return thirdPartId;
    }

    /**
     * 设置第三方id
     *
     * @param thirdPartId 第三方id
     */
    public void setThirdPartId(String thirdPartId) {
        this.thirdPartId = thirdPartId == null ? null : thirdPartId.trim();
    }

    /**
     * 获取用户来源
     *
     * @return source_channel - 用户来源
     */
    public String getSourceChannel() {
        return sourceChannel;
    }

    /**
     * 设置用户来源
     *
     * @param sourceChannel 用户来源
     */
    public void setSourceChannel(String sourceChannel) {
        this.sourceChannel = sourceChannel == null ? null : sourceChannel.trim();
    }

    /**
     * 获取用户姓名
     *
     * @return customername - 用户姓名
     */
    public String getCustomername() {
        return customername;
    }

    /**
     * 设置用户姓名
     *
     * @param customername 用户姓名
     */
    public void setCustomername(String customername) {
        this.customername = customername == null ? null : customername.trim();
    }

    /**
     * 获取用户姓别
     *
     * @return gender - 用户姓别
     */
    public String getGender() {
        return gender;
    }

    /**
     * 设置用户姓别
     *
     * @param gender 用户姓别
     */
    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    /**
     * 获取婚姻状态
     *
     * @return marital - 婚姻状态
     */
    public String getMarital() {
        return marital;
    }

    /**
     * 设置婚姻状态
     *
     * @param marital 婚姻状态
     */
    public void setMarital(String marital) {
        this.marital = marital == null ? null : marital.trim();
    }

    /**
     * 获取生日
     *
     * @return birth - 生日
     */
    public Date getBirth() {
        return birth;
    }

    /**
     * 设置生日
     *
     * @param birth 生日
     */
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    /**
     * 获取所在省
     *
     * @return province - 所在省
     */
    public String getProvince() {
        return province;
    }

    /**
     * 设置所在省
     *
     * @param province 所在省
     */
    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    /**
     * 获取所在城市
     *
     * @return city - 所在城市
     */
    public String getCity() {
        return city;
    }

    /**
     * 设置所在城市
     *
     * @param city 所在城市
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * 获取所在地区
     *
     * @return district - 所在地区
     */
    public String getDistrict() {
        return district;
    }

    /**
     * 设置所在地区
     *
     * @param district 所在地区
     */
    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    /**
     * 获取具体地址
     *
     * @return address - 具体地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设置具体地址
     *
     * @param address 具体地址
     */
    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    /**
     * 获取邮编
     *
     * @return zip - 邮编
     */
    public String getZip() {
        return zip;
    }

    /**
     * 设置邮编
     *
     * @param zip 邮编
     */
    public void setZip(String zip) {
        this.zip = zip == null ? null : zip.trim();
    }

    /**
     * 获取注册时间
     *
     * @return register_time - 注册时间
     */
    public Date getRegisterTime() {
        return registerTime;
    }

    /**
     * 设置注册时间
     *
     * @param registerTime 注册时间
     */
    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * 获取上次登录时间
     *
     * @return last_login_time - 上次登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置上次登录时间
     *
     * @param lastLoginTime 上次登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * 获取积分
     *
     * @return user_point - 积分
     */
    public Integer getUserPoint() {
        return userPoint;
    }

    /**
     * 设置积分
     *
     * @param userPoint 积分
     */
    public void setUserPoint(Integer userPoint) {
        this.userPoint = userPoint;
    }

    /**
     * 获取用户余额
     *
     * @return user_money - 用户余额
     */
    public BigDecimal getUserMoney() {
        return userMoney;
    }

    /**
     * 设置用户余额
     *
     * @param userMoney 用户余额
     */
    public void setUserMoney(BigDecimal userMoney) {
        this.userMoney = userMoney;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}