package io.csl.modules.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "tb_transaction_log")
public class TransactionLog {
    @Id
    private Long rid;

    /**
     * 交易标识
     */
    @Column(name = "txn_id")
    private String txnId;

    /**
     * 事件类型
     */
    @Column(name = "events_type")
    private String eventsType;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 创建人
     */
    @Column(name = "create_by")
    private String createBy;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 更新人
     */
    @Column(name = "update_by")
    private String updateBy;

    /**
     * 事件详情
     */
    private String events;

    /**
     * 结果详情
     */
    private String results;

    /**
     * @return rid
     */
    public Long getRid() {
        return rid;
    }

    /**
     * @param rid
     */
    public void setRid(Long rid) {
        this.rid = rid;
    }

    /**
     * 获取交易标识
     *
     * @return txn_id - 交易标识
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * 设置交易标识
     *
     * @param txnId 交易标识
     */
    public void setTxnId(String txnId) {
        this.txnId = txnId == null ? null : txnId.trim();
    }

    /**
     * 获取事件类型
     *
     * @return events_type - 事件类型
     */
    public String getEventsType() {
        return eventsType;
    }

    /**
     * 设置事件类型
     *
     * @param eventsType 事件类型
     */
    public void setEventsType(String eventsType) {
        this.eventsType = eventsType == null ? null : eventsType.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取创建人
     *
     * @return create_by - 创建人
     */
    public String getCreateBy() {
        return createBy;
    }

    /**
     * 设置创建人
     *
     * @param createBy 创建人
     */
    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取更新人
     *
     * @return update_by - 更新人
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置更新人
     *
     * @param updateBy 更新人
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    /**
     * 获取事件详情
     *
     * @return events - 事件详情
     */
    public String getEvents() {
        return events;
    }

    /**
     * 设置事件详情
     *
     * @param events 事件详情
     */
    public void setEvents(String events) {
        this.events = events == null ? null : events.trim();
    }

    /**
     * 获取结果详情
     *
     * @return results - 结果详情
     */
    public String getResults() {
        return results;
    }

    /**
     * 设置结果详情
     *
     * @param results 结果详情
     */
    public void setResults(String results) {
        this.results = results == null ? null : results.trim();
    }
}