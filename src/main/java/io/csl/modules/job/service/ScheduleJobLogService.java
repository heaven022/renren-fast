package io.csl.modules.job.service;

import com.baomidou.mybatisplus.extension.service.IService;

import io.csl.common.utils.PageUtils;
import io.csl.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;

/**
 * 定时任务日志
 *
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
}
