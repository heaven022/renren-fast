
package io.csl.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;

import io.csl.common.utils.PageUtils;
import io.csl.modules.sys.entity.SysLogEntity;

import java.util.Map;


/**
 * 系统日志
 *
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
