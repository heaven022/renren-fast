-- 菜单
CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL AUTO_INCREMENT,
  `parent_id` bigint COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) COMMENT '菜单名称',
  `url` varchar(200) COMMENT '菜单URL',
  `perms` varchar(500) COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) COMMENT '菜单图标',
  `order_num` int COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- 系统用户
CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) COMMENT '密码',
  `salt` varchar(20) COMMENT '盐',
  `email` varchar(100) COMMENT '邮箱',
  `mobile` varchar(100) COMMENT '手机号',
  `status` tinyint COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) COMMENT '创建者ID',
  `create_time` datetime COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- 系统用户Token
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户Token';

-- 系统验证码
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统验证码';

-- 角色
CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) COMMENT '角色名称',
  `remark` varchar(100) COMMENT '备注',
  `create_user_id` bigint(20) COMMENT '创建者ID',
  `create_time` datetime COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';

-- 用户与角色对应关系
CREATE TABLE `sys_user_role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint COMMENT '用户ID',
  `role_id` bigint COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- 角色与菜单对应关系
CREATE TABLE `sys_role_menu` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_id` bigint COMMENT '角色ID',
  `menu_id` bigint COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- 系统配置信息
CREATE TABLE `sys_config` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`param_key` varchar(50) COMMENT 'key',
	`param_value` varchar(2000) COMMENT 'value',
	`status` tinyint DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
	`remark` varchar(500) COMMENT '备注',
	PRIMARY KEY (`id`),
	UNIQUE INDEX (`param_key`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='系统配置信息表';


-- 系统日志
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COMMENT '用户名',
  `operation` varchar(50) COMMENT '用户操作',
  `method` varchar(200) COMMENT '请求方法',
  `params` varchar(5000) COMMENT '请求参数',
  `time` bigint NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) COMMENT 'IP地址',
  `create_date` datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='系统日志';


-- 文件上传
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) COMMENT 'URL地址',
  `create_date` datetime COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=`InnoDB` DEFAULT CHARACTER SET utf8 COMMENT='文件上传';


-- 定时任务
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- 定时任务日志
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务日志';



-- 用户表
CREATE TABLE `tb_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) COMMENT '密码',
  `create_time` datetime COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

-- 组织机构表
CREATE TABLE `tb_organization` (
  `org_id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL COMMENT '店名',
  `header` varchar(500) NOT NULL COMMENT '负责人',
  `mobile` varchar(20) NOT NULL COMMENT '负责人手机号',
  `province` varchar(20) COMMENT '所在省',
  `city` varchar(20) COMMENT '所在城市',
  `district` varchar(20) COMMENT '所在地区',
  `address` varchar(200) COMMENT '具体地址',
  `zip` varchar(20) COMMENT '邮编',
  `ing` decimal(10, 7) COMMENT '经度',
  `lat` decimal(10, 7) COMMENT '维度',
  `org_type` varchar(20) COMMENT '店铺类型',
  `status` varchar(2) COMMENT '状态',
  `pid` bigint COMMENT '父id',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`org_id`),
  UNIQUE INDEX (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='组织机构表';


-- 设备表
CREATE TABLE `tb_device` (
  `dev_id` bigint NOT NULL AUTO_INCREMENT,
  `device_type` varchar(2) NOT NULL COMMENT '硬件类型',
  `device_status` varchar(2) NOT NULL COMMENT '状态',
  `org_id` bigint COMMENT '所属门店',
  `field1` varchar(200) COMMENT '设备参数',
  `field2` varchar(200) COMMENT '设备参数',
  `field3` varchar(200) COMMENT '设备参数',
  `field4` varchar(200) COMMENT '设备参数',
  `field5` varchar(200) COMMENT '设备参数',
  `URL` varchar(2000) COMMENT '监控URL',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`dev_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备';

-- 增加sys_user的字段关联组织表当为空时为超级管理员
alter table sys_user add org_id bigint;

-- 增加tb_user中的字段
alter table tb_user add third_part_id varchar(200) COMMENT '第三方id';
alter table tb_user add source_channel varchar(2) COMMENT '用户来源';
alter table tb_user add customername varchar(100) COMMENT '用户姓名';
alter table tb_user add gender varchar(2) COMMENT '用户姓别';
alter table tb_user add marital varchar(2) COMMENT '婚姻状态';
alter table tb_user add birth date COMMENT '生日';
alter table tb_user add province varchar(20) COMMENT '所在省';
alter table tb_user add city varchar(20) COMMENT '所在城市';
alter table tb_user add district varchar(20) COMMENT '所在地区';
alter table tb_user add address varchar(200) COMMENT '具体地址';
alter table tb_user add zip varchar(20) COMMENT '邮编';
alter table tb_user add register_time datetime COMMENT '注册时间';
alter table tb_user add last_login_time datetime COMMENT '上次登录时间';
alter table tb_user add user_point INT COMMENT '积分';
alter table tb_user add user_money DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '用户余额';
alter table tb_user add create_by varchar(20) COMMENT '创建人';
alter table tb_user add update_time datetime COMMENT '更新时间';
alter table tb_user add update_by varchar(20) COMMENT '更新人';

-- 会员级别表
CREATE TABLE `tb_customer_level_inf` (
  `lvl_id` bigint NOT NULL AUTO_INCREMENT,
  `level_name` varchar(20) NOT NULL COMMENT '级别名称',
  `min_point` INT UNSIGNED  COMMENT '该级别最低积分',
  `max_point` INT UNSIGNED  COMMENT '该级别最高积分',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`lvl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员级别表';


-- 会员积分变动表
CREATE TABLE `tb_customer_point_log` (
  `point_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `source` varchar(10)  COMMENT '来源：0 订单 1 登录 2活动 3充值',
  `refer_number` varchar(100) COMMENT '来源标识编号',
  `change_point` INT COMMENT '变更积分数',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  PRIMARY KEY (`point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员积分变动表';

-- 会员用户余额变动表(用户余额变动表，记录余额充值和消费)
CREATE TABLE `tb_customer_ balance_log` (
  `balance_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `source` varchar(10)  COMMENT '来源：1 充值 2 消费 3退款 4转回微信',
  `refer_number` varchar(100) COMMENT '来源标识编号',
  `status` varchar(2) COMMENT '技术状态',
  `remark` varchar(200) COMMENT '描述备注',
  `amount` DECIMAL(10,2) COMMENT '变更金额',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`balance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员用户余额变动表';

-- 用户登录记录表
CREATE TABLE `tb_customer_login_log` (
  `login_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL COMMENT '用户id',
  `login_time` datetime  COMMENT '登录时间',
  `login_ip` varchar(100) COMMENT 'IP',
  `login_type` varchar(2) COMMENT '登录类型',
  `logout_time` datetime COMMENT '登出',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户登录记录表';

-- 用户汽车表
CREATE TABLE `tb_car` (
  `car_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint COMMENT '用户id',
  `car_license_num` varchar(100) COMMENT '车牌号',
  `car_brand` varchar(100) COMMENT '子品牌',
  `car_company` varchar(100) COMMENT '父品牌',
  `car_type` varchar(2) COMMENT '类型',
  `major_car` varchar(2) COMMENT '是否为主汽车',
  `status` varchar(2)  COMMENT '状态',
  `third_part_id` varchar(100)  COMMENT '第三方标识',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户汽车表';


-- 优惠券表
CREATE TABLE `tb_promotion_type` (
  `pro_id` bigint NOT NULL AUTO_INCREMENT,
  `amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '优惠金额',
  `type` varchar(2) COMMENT '优惠券类型：1 优惠金额 2 全免 3 折扣',
  `exipredate` INT COMMENT '有效期：天',
  `status` varchar(2) COMMENT '状态',
   `field1` varchar(200) COMMENT '参数',
  `field2` varchar(200) COMMENT '参数',
  `field3` varchar(200) COMMENT '参数',
  `field4` varchar(200) COMMENT '参数',
  `field5` varchar(200) COMMENT '参数',
  `car_type` varchar(2) COMMENT '类型',
  `start_date` datetime COMMENT '开始时间',
  `end_date` datetime COMMENT '结束时间',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`pro_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券表';


-- 用户优惠码优惠券关系表
CREATE TABLE `tb_customer_promotion_rel` (
  `rid` bigint NOT NULL AUTO_INCREMENT,
  `pid` bigint COMMENT '优惠券类型id',
  `user_id` bigint COMMENT '用户id',
  `third_part_id` varchar(100) COMMENT '第三方标识',
  `promotion_code` varchar(100) NOT NULL COMMENT '优惠券码：唯一',
  `status` varchar(2) COMMENT '状态',
  `reference_sn` varchar(200) COMMENT '关联订单号',
  `remark` varchar(200) COMMENT '备注',
  `used_date` datetime COMMENT '使用时间',
  `expire_date` datetime COMMENT '过期时间',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户优惠码优惠券关系表';

-- 用户订单表
CREATE TABLE `tb_order` (
  `oid` bigint NOT NULL AUTO_INCREMENT,
  `store_id` bigint NOT NULL COMMENT '交易商铺',
  `device_id` varchar(100) COMMENT '交易设备',
  `pro_r_id` bigint COMMENT '优惠券的主键',
  `user_id` bigint COMMENT '用户id',
  `third_part_id` varchar(100) COMMENT '第三方标识',
  `remark` varchar(300) COMMENT '备注',
  `txn_id` varchar(100) COMMENT '内部交易id',
  `payment_method` varchar(2) COMMENT '交易方式类型',
  `order_type` varchar(2) COMMENT '订单类型',
  `txn_desp` varchar(200) COMMENT '交易描述',
  `order_amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '订单总金额',
  `integral` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '使用余额结算金额',
  `promotion_amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '使用优惠券结算金额',
  `payment_amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '第三方支付需要结算金额',
  `invoice` varchar(2) COMMENT '是否开过发票',
  `promotion_code` varchar(100) NOT NULL COMMENT '优惠券码：唯一',
  `status` varchar(2) COMMENT '状态',
  `reference_sn` varchar(200) COMMENT '第三方关联订单号',
  `error_code` varchar(200) COMMENT '错误代码',
  `error_desc` varchar(200) COMMENT '错误描述',
  `field1` varchar(200) COMMENT '参数',
  `field2` varchar(200) COMMENT '参数',
  `field3` varchar(200) COMMENT '参数',
  `field4` varchar(200) COMMENT '参数',
  `field5` varchar(200) COMMENT '参数',
  `closed_date` datetime COMMENT '对账结算时间',
  `txn_date` datetime COMMENT '交易完成时间',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户订单表';

-- 交易事件表
CREATE TABLE `tb_transaction_log` (
  `rid` bigint NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(100) NOT NULL COMMENT '交易标识',
  `events` text COLLATE utf8mb4_unicode_ci NOT NULL  COMMENT '事件详情',
  `results` text COLLATE utf8mb4_unicode_ci COMMENT '结果详情',
  `events_type` varchar(2) NOT NULL COMMENT '事件类型',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交易事件表';

-- 发票信息表
CREATE TABLE `tb_user_invoice_info` (
  `info_id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint COMMENT '用户id',
  `is_vat` varchar(2) COMMENT '是否为增值税发票',
  `invoice_title` varchar(200) COMMENT '发票抬头名称',
  `invoice_content` varchar(200) COMMENT '发票抬头内容',
  `invoice_tax_no` varchar(200) COMMENT '发票税号',
  `vat_company_name` varchar(400) COMMENT '公司名称[增值税]',
  `vat_company_address` varchar(400) COMMENT '公司地址[增值税]',
  `vat_telphone` varchar(20) COMMENT '联系电话[增值税]',
  `vat_bank_name` varchar(400) COMMENT '开户银行[增值税]',
  `vat_bank_account` varchar(50) COMMENT '银行帐号[增值税]',
  `status` varchar(2) COMMENT '状态',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发票信息表';

-- 实体发票表
CREATE TABLE `tb_order_invoice_info` (
  `vid` bigint NOT NULL AUTO_INCREMENT,
  `info_id` bigint COMMENT '发票信息id',
  `invoice_amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '发票金额',
  `tax_amount` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '税金金额',
  `fixed_time` datetime COMMENT '开票时间',
  `status` varchar(2) COMMENT '状态',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体发票表';

-- 发票订单关系表
CREATE TABLE `tb_order_invoice_rel` (
  `rid` bigint NOT NULL AUTO_INCREMENT,
  `vid` bigint COMMENT '发票实体id',
  `order_id` bigint COMMENT '订单id',
  `status` varchar(2) COMMENT '状态',
  `create_time` datetime COMMENT '创建时间',
  `create_by` varchar(20) COMMENT '创建人',
  `update_time` datetime COMMENT '更新时间',
  `update_by` varchar(20) COMMENT '更新人',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='发票订单关系表';


-- 初始数据 
INSERT INTO `sys_user` (`user_id`, `username`, `password`, `salt`, `email`, `mobile`, `status`, `create_user_id`, `create_time`) VALUES ('1', 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', '878236523@qq.com', '13820765934', '1', '1', '2016-11-11 11:11:11');

INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (5, 1, 'SQL监控', 'http://localhost:8080/csl-fast/druid/sql.html', NULL, 1, 'sql', 4);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (6, 1, '定时任务', 'job/schedule', NULL, 1, 'job', 5);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (7, 6, '查看', NULL, 'sys:schedule:list,sys:schedule:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (8, 6, '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (9, 6, '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (10, 6, '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (11, 6, '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (12, 6, '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (13, 6, '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (14, 6, '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7);
INSERT INTO `sys_menu`(`menu_id`, `parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`) VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'oss', 6);

INSERT INTO `sys_config` (`param_key`, `param_value`, `status`, `remark`) VALUES ('CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', '0', '云存储配置信息');
INSERT INTO `schedule_job` (`bean_name`, `params`, `cron_expression`, `status`, `remark`, `create_time`) VALUES ('testTask', 'renren', '0 0/30 * * * ?', '0', '参数测试', now());


-- 账号：13612345678  密码：admin
INSERT INTO `tb_user` (`username`, `mobile`, `password`, `create_time`) VALUES ('mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');








--  quartz自带表结构
CREATE TABLE QRTZ_JOB_DETAILS(
SCHED_NAME VARCHAR(120) NOT NULL,
JOB_NAME VARCHAR(200) NOT NULL,
JOB_GROUP VARCHAR(200) NOT NULL,
DESCRIPTION VARCHAR(250) NULL,
JOB_CLASS_NAME VARCHAR(250) NOT NULL,
IS_DURABLE VARCHAR(1) NOT NULL,
IS_NONCONCURRENT VARCHAR(1) NOT NULL,
IS_UPDATE_DATA VARCHAR(1) NOT NULL,
REQUESTS_RECOVERY VARCHAR(1) NOT NULL,
JOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
JOB_NAME VARCHAR(200) NOT NULL,
JOB_GROUP VARCHAR(200) NOT NULL,
DESCRIPTION VARCHAR(250) NULL,
NEXT_FIRE_TIME BIGINT(13) NULL,
PREV_FIRE_TIME BIGINT(13) NULL,
PRIORITY INTEGER NULL,
TRIGGER_STATE VARCHAR(16) NOT NULL,
TRIGGER_TYPE VARCHAR(8) NOT NULL,
START_TIME BIGINT(13) NOT NULL,
END_TIME BIGINT(13) NULL,
CALENDAR_NAME VARCHAR(200) NULL,
MISFIRE_INSTR SMALLINT(2) NULL,
JOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_SIMPLE_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
REPEAT_COUNT BIGINT(7) NOT NULL,
REPEAT_INTERVAL BIGINT(12) NOT NULL,
TIMES_TRIGGERED BIGINT(10) NOT NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_CRON_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
CRON_EXPRESSION VARCHAR(120) NOT NULL,
TIME_ZONE_ID VARCHAR(80),
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_SIMPROP_TRIGGERS
  (          
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 BIGINT NULL,
    LONG_PROP_2 BIGINT NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
    REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_BLOB_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
BLOB_DATA BLOB NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
INDEX (SCHED_NAME,TRIGGER_NAME, TRIGGER_GROUP),
FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_CALENDARS (
SCHED_NAME VARCHAR(120) NOT NULL,
CALENDAR_NAME VARCHAR(200) NOT NULL,
CALENDAR BLOB NOT NULL,
PRIMARY KEY (SCHED_NAME,CALENDAR_NAME))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS (
SCHED_NAME VARCHAR(120) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_FIRED_TRIGGERS (
SCHED_NAME VARCHAR(120) NOT NULL,
ENTRY_ID VARCHAR(95) NOT NULL,
TRIGGER_NAME VARCHAR(200) NOT NULL,
TRIGGER_GROUP VARCHAR(200) NOT NULL,
INSTANCE_NAME VARCHAR(200) NOT NULL,
FIRED_TIME BIGINT(13) NOT NULL,
SCHED_TIME BIGINT(13) NOT NULL,
PRIORITY INTEGER NOT NULL,
STATE VARCHAR(16) NOT NULL,
JOB_NAME VARCHAR(200) NULL,
JOB_GROUP VARCHAR(200) NULL,
IS_NONCONCURRENT VARCHAR(1) NULL,
REQUESTS_RECOVERY VARCHAR(1) NULL,
PRIMARY KEY (SCHED_NAME,ENTRY_ID))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_SCHEDULER_STATE (
SCHED_NAME VARCHAR(120) NOT NULL,
INSTANCE_NAME VARCHAR(200) NOT NULL,
LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
CHECKIN_INTERVAL BIGINT(13) NOT NULL,
PRIMARY KEY (SCHED_NAME,INSTANCE_NAME))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE QRTZ_LOCKS (
SCHED_NAME VARCHAR(120) NOT NULL,
LOCK_NAME VARCHAR(40) NOT NULL,
PRIMARY KEY (SCHED_NAME,LOCK_NAME))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX IDX_QRTZ_J_REQ_RECOVERY ON QRTZ_JOB_DETAILS(SCHED_NAME,REQUESTS_RECOVERY);
CREATE INDEX IDX_QRTZ_J_GRP ON QRTZ_JOB_DETAILS(SCHED_NAME,JOB_GROUP);

CREATE INDEX IDX_QRTZ_T_J ON QRTZ_TRIGGERS(SCHED_NAME,JOB_NAME,JOB_GROUP);
CREATE INDEX IDX_QRTZ_T_JG ON QRTZ_TRIGGERS(SCHED_NAME,JOB_GROUP);
CREATE INDEX IDX_QRTZ_T_C ON QRTZ_TRIGGERS(SCHED_NAME,CALENDAR_NAME);
CREATE INDEX IDX_QRTZ_T_G ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_GROUP);
CREATE INDEX IDX_QRTZ_T_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_STATE);
CREATE INDEX IDX_QRTZ_T_N_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE);
CREATE INDEX IDX_QRTZ_T_N_G_STATE ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE);
CREATE INDEX IDX_QRTZ_T_NEXT_FIRE_TIME ON QRTZ_TRIGGERS(SCHED_NAME,NEXT_FIRE_TIME);
CREATE INDEX IDX_QRTZ_T_NFT_ST ON QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME);
CREATE INDEX IDX_QRTZ_T_NFT_MISFIRE ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME);
CREATE INDEX IDX_QRTZ_T_NFT_ST_MISFIRE ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE);
CREATE INDEX IDX_QRTZ_T_NFT_ST_MISFIRE_GRP ON QRTZ_TRIGGERS(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_FT_TRIG_INST_NAME ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,INSTANCE_NAME);
CREATE INDEX IDX_QRTZ_FT_INST_JOB_REQ_RCVRY ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY);
CREATE INDEX IDX_QRTZ_FT_J_G ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,JOB_NAME,JOB_GROUP);
CREATE INDEX IDX_QRTZ_FT_JG ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,JOB_GROUP);
CREATE INDEX IDX_QRTZ_FT_T_G ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP);
CREATE INDEX IDX_QRTZ_FT_TG ON QRTZ_FIRED_TRIGGERS(SCHED_NAME,TRIGGER_GROUP);
